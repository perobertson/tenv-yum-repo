# tenv-yum-repo

This will create a local yum repo for `tenv`.
It also creates an update service to keep it up to date.

## Requirements

- createrepo_c
- curl
- jq
- rpm
- sed

## Usage

```bash
sudo make install
```
