#!/usr/bin/env bash
set -euo pipefail

LATEST_VERSION_TAG=$(curl --silent https://api.github.com/repos/tofuutils/tenv/releases/latest | jq -r .tag_name)
# allow the use of sed here due to the regex
# shellcheck disable=SC2001
LATEST_VERSION=$(echo "${LATEST_VERSION_TAG}" | sed 's/^v//')
rpm_url="https://github.com/tofuutils/tenv/releases/download/${LATEST_VERSION_TAG}/tenv_${LATEST_VERSION_TAG}_amd64.rpm"

# Check if the rpm was already downloaded
if ! ls -1 /var/lib/tenv-yum-repo/*"${LATEST_VERSION}-1"*.rpm; then
    echo "Downloading new version: ${LATEST_VERSION_TAG}"

    # Download the rpm
    tmp_rpm="/var/tmp/tenv_${LATEST_VERSION_TAG}_amd64.rpm"
    curl -sSL --output "${tmp_rpm}" "${rpm_url}"

    # Find out what the actual version is
    rpm_version=$(rpm --query "${tmp_rpm}")

    # Add the new version to the yum repo and preserve destination SELinux context
    cp "${tmp_rpm}" "/var/lib/tenv-yum-repo/${rpm_version}.rpm"
    rm "${tmp_rpm}"
fi

# Update the yum repo data
echo "Updating yum repo data"
createrepo_c /var/lib/tenv-yum-repo/
