.DEFAULT_GOAL := install

/etc/yum.repos.d/tenv-local.repo: files/etc/yum.repos.d/tenv-local.repo
/etc/yum.repos.d/tenv-local.repo: /var/lib/tenv-yum-repo
	install -m 0644 \
		files/etc/yum.repos.d/tenv-local.repo \
		/etc/yum.repos.d/tenv-local.repo

/usr/lib/systemd/system-preset/10-tenv-yum-repo-update.preset: files/usr/lib/systemd/system-preset/10-tenv-yum-repo-update.preset
/usr/lib/systemd/system-preset/10-tenv-yum-repo-update.preset: /usr/lib/systemd/system/tenv-yum-repo-update.timer
	install -m 0644 \
		files/usr/lib/systemd/system-preset/10-tenv-yum-repo-update.preset \
		/usr/lib/systemd/system-preset/10-tenv-yum-repo-update.preset
	systemctl daemon-reload

/usr/lib/systemd/system/tenv-yum-repo-update.service: files/usr/lib/systemd/system/tenv-yum-repo-update.service
/usr/lib/systemd/system/tenv-yum-repo-update.service: /usr/local/bin/tenv-yum-repo-update.bash
	install -m 0644 \
		files/usr/lib/systemd/system/tenv-yum-repo-update.service \
		/usr/lib/systemd/system/
	systemctl daemon-reload

/usr/lib/systemd/system/tenv-yum-repo-update.timer: files/usr/lib/systemd/system/tenv-yum-repo-update.timer
/usr/lib/systemd/system/tenv-yum-repo-update.timer: /usr/lib/systemd/system/tenv-yum-repo-update.service
	install -m 0644 \
		files/usr/lib/systemd/system/tenv-yum-repo-update.timer \
		/usr/lib/systemd/system/
	systemctl daemon-reload

/usr/local/bin/tenv-yum-repo-update.bash: files/usr/local/bin/tenv-yum-repo-update.bash
/usr/local/bin/tenv-yum-repo-update.bash: /var/lib/tenv-yum-repo
	install -m 0755 \
		files/usr/local/bin/tenv-yum-repo-update.bash \
		/usr/local/bin/

/var/lib/tenv-yum-repo:
	mkdir -p /var/lib/tenv-yum-repo

.PHONY: install
install:
	$(MAKE) install-service
	$(MAKE) start-service
	while systemctl is-active tenv-yum-repo-update.service; do sleep 5; done
	$(MAKE) install-tenv

.PHONY: install-repo
install-repo: /etc/yum.repos.d/tenv-local.repo

.PHONY: install-script
install-script: /usr/local/bin/tenv-yum-repo-update.bash

.PHONY: install-service
install-service: install-repo
	$(MAKE) /usr/lib/systemd/system-preset/10-tenv-yum-repo-update.preset
	systemctl preset tenv-yum-repo-update.timer

.PHONY: install-tenv
install-tenv:
	dnf makecache --repo tenv-local
	dnf install tenv
	dnf upgrade tenv

.PHONY: start-service
start-service:
	systemctl start tenv-yum-repo-update.service

.PHONY: enable-timer
enable-timer:
	systemctl enable tenv-yum-repo-update.timer

.PHONY: start-timer
start-timer:
	systemctl start tenv-yum-repo-update.timer
